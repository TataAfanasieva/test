
describe('POST API Log In', () => {
  it('POST successfully', () => {
      console.log(Cypress.env('BASE_URL'))
        cy.request({
            method: 'POST',
            url: Cypress.env('BASE_API_URL') + '/api/v3/users/auth/login',
            body:{
              "email": "test@koyfin.com",
              "password": "12345abc"
          },
            headers: {
                'Cookie': 'cookieValueGoesHere',
                'Content-Type':'application/json'
            }
        }).then((response) => {

          expect(response.status).to.eq(201)
  
        })
        
   })
  })
