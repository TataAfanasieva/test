import moment from 'moment';

describe('Equity Historical graph', () => {

    it('POST', () => {
          
        let options; //1
        let webSocketResponse;

        ["eq-o1antf", "eq-xxf5is", "eq-8o3iqk", "eq-ngp1q3", "eq-8ragq3", "eq-r07fqf",
         "eq-awglra", "eq-212q1o", "eq-gu8g06", "eq-5suzv9", "eq-r0n49j", "eq-58ui4m",
         "eq-k0d1vn", "eq-hqkokn", "eq-852mvw", "eq-7n04a1", "eq-l1rtsf", "eq-6a2k31", 
         "eq-hmiy8d", "eq-knpqnf","eq-o1yp8a", "eq-8ya6k5", "eq-kayi2e", "eq-7kbr1j",
         "eq-v9iudl", "eq-mp7ff3", "eq-16pm0p", "eq-gu8g06", "eq-5mtro9" ].forEach(id => {
          
          const config = {
            url: "wss://ws.koyfin.com/v2/live-data?tickers=" + id
          };

          cy.streamRequest(config, options).then(results => {
            expect(results).to.not.be.undefined;
            webSocketResponse = results;
          });

          cy.request({
            method: 'POST',
            url: Cypress.env('BASE_API_URL') + '/api/v3/data/graph',
            body: {
              "id": id,
              "key": "p_candle_range",
              "useAdjustedPrice": true,
              "priceFormat": "both",
              "candleAggregationPeriod": "day",
              "dateFrom": "2021-03-01",
              "dateTo": "2025-02-04"
            },
            headers: {
              'Cookie': 'cookieValueGoesHere',
              'Content-Type':'application/json'
            }
            }).should((response) => {

              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).to.have.property('graph');

              const graph = response.body.graph;
              const lastData = graph[graph.length - 1];
              const today = moment().format('YYYY-MM-DD');

             // expect(lastData.date).to.equal(today);
              cy.log(response);

              //expect(webSocketResponse[0][6]).to.equal(lastData.adjOpen);
              expect(webSocketResponse[0][9]).to.equal(lastData.adjClose);

              cy.writeFile("cypress/fixtures/post_response.json", response.body)

            })
        });

     })
    })