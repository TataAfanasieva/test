describe('POST API MFs Meta keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "mf-mzon4g"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "t_n"
                    },
                    {
                        "key": "p_l"
                    },
                    {
                        "key": "mf_12b1"
                    },
                    {
                        "key": "mf_yield_30d"
                    },
                    {
                        "key": "mf_administrator"
                    },
                    {
                        "key": "mf_asset_type"
                    },
                    {
                        "key": "mf_broad_benchmark"
                    },
                    {
                        "key": "mf_classification"
                    },
                    {
                        "key": "mf_custodian"
                    },
                    {
                        "key": "mf_distrib_freq"
                    },
                    {
                        "key": "mf_dps"
                    },
                    {
                        "key": "mf_domicile"
                    },
                    {
                        "key": "mf_full_name"
                    },
                    {
                        "key": "mf_fund_benchmark"
                    },
                    {
                        "key": "mf_lipper"
                    },
                    {
                        "key": "mf_fund_manager_co"
                    },
                    {
                        "key": "mf_fund_objective"
                    },
                    {
                        "key": "mf_roa_avg_1y"
                    },
                    {
                        "key": "mf_roe_avg_1y"
                    },
                    {
                        "key": "mf_roi_avg_1y"
                    },
                    {
                        "key": "mf_eps_cagr_1y"
                    },
                    {
                        "key": "mf_rps_cagr_1y"
                    },
                    {
                        "key": "mf_sales_cagr_1y"
                    },
                    {
                        "key": "mf_dps_3y"
                    },
                    {
                        "key": "mf_roa_avg_3y"
                    },
                    {
                        "key": "mf_roc_3y"
                    },
                    {
                        "key": "mf_roe_avg_3y"
                    },
                    {
                        "key": "mf_roi_avg_3y"
                    },
                    {
                        "key": "mf_eps_cagr_3y"
                    },
                    {
                        "key": "mf_ocf_cagr_3y"
                    },
                    {
                        "key": "mf_rps_cagr_3y"
                    },
                    {
                        "key": "mf_sales_cagr_3y"
                    },
                    {
                        "key": "mf_sales_cagr_5y"
                    },
                    {
                        "key": "mf_eps_5y"
                    },
                    {
                        "key": "mf_sales_ratio_cagr_3y"
                    },
                    {
                        "key": "mf_payout_ratio_cagr_5y"
                    },
                    {
                        "key": "mf_assets_equity"
                    },
                    {
                        "key": "mf_avg_coupon"
                    },
                    {
                        "key": "mf_avg_mkt_cap_lg_pct"
                    },
                    {
                        "key": "mf_avg_quality"
                    },
                    {
                        "key": "mf_avg_mkt_cap_mid_pct"
                    },
                    {
                        "key": "mf_avg_mkt_cap_sm_pct"
                    },
                    {
                        "key": "mf_debt_capital"
                    },
                    {
                        "key": "mf_debt_shareholders"
                    },
                    {
                        "key": "mf_debt_equity"
                    },
                    {
                        "key": "mf_ebit_int"
                    },
                    {
                        "key": "mf_eff_maturity"
                    },
                    {
                        "key": "mf_pb_l"
                    },
                    {
                        "key": "mf_pe_l"
                    },
                    {
                        "key": "mf_ps_l"
                    },
                    {
                        "key": "mf_nominal_maturity"
                    },
                    {
                        "key": "mf_payout_ratio"
                    },
                    {
                        "key": "mf_pb"
                    },
                    {
                        "key": "mf_pc"
                    },
                    {
                        "key": "mf_pd"
                    },
                    {
                        "key": "mf_pe"
                    },
                    {
                        "key": "mf_ps"
                    },
                    {
                        "key": "mf_rsi"
                    },
                    {
                        "key": "mf_roc"
                    },
                    {
                        "key": "mf_dy_wavg"
                    },
                    {
                        "key": "mf_mktcap_wavg"
                    },
                    {
                        "key": "mf_roe_wavg"
                    },
                    {
                        "key": "mf_ytm"
                    },
                    {
                        "key": "mf_zscore"
                    },
                    {
                        "key": "mf_aggtna"
                    },
                    {
                        "key": "mf_monthendtna"
                    },
                    {
                        "key": "mf_fund_type"
                    },
                    {
                        "key": "mf_geo_focus"
                    },
                    {
                        "key": "mf_ipo_date"
                    },
                    {
                        "key": "mf_ipo_price"
                    },
                    {
                        "key": "mf_inception_date"
                    },
                    {
                        "key": "mf_investment_advisor"
                    },
                    {
                        "key": "mf_launch_date"
                    },
                    {
                        "key": "mf_launch_price"
                    },
                    {
                        "key": "mf_legal_name"
                    },
                    {
                        "key": "mf_load_type"
                    },
                    {
                        "key": "mf_mgmt_fees"
                    },
                    {
                        "key": "mf_max_sales_charge"
                    },
                    {
                        "key": "mf_min_invest_initial"
                    },
                    {
                        "key": "mf_min_invest_initial_ira"
                    },
                    {
                        "key": "mf_min_invest_regular"
                    },
                    {
                        "key": "mf_min_invest_sub_ira"
                    },
                    {
                        "key": "mf_net_exp_fees"
                    },
                    {
                        "key": "mf_non_mgmt_fees"
                    },
                    {
                        "key": "mf_peer_benchmark"
                    },
                    {
                        "key": "mf_turnover"
                    },
                    {
                        "key": "mf_yield_sec"
                    },
                    {
                        "key": "mf_share_class"
                    },
                    {
                        "key": "mf_avgnetassets"
                    },
                    {
                        "key": "mf_tot_exp_fees"
                    },
                    {
                        "key": "mf_shares_outstanding"
                    },
                    {
                        "key": "mf_transfer_agent"
                    },
                    {
                        "key": "mf_asset_universe"
                    },
                    {
                        "key": "t_cat"
                    },
                    {
                        "key": "t_u"
                    },
                    {
                        "key": "t_id"
                    }
                ]
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })