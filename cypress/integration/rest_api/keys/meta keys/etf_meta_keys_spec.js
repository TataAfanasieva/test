describe('POST API ETF Meta keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "et-n5kqqt"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "t_n"
                    },
                    {
                        "key": "p_l"
                    },
                    {
                        "key": "t_cat"
                    },
                    {
                        "key": "company_country"
                    },
                    {
                        "key": "trading_country"
                    },
                    {
                        "key": "t_ex"
                    },
                    {
                        "key": "t_freq"
                    },
                    {
                        "key": "t_source"
                    },
                    {
                        "key": "t_u"
                    },
                    {
                        "key": "t_id"
                    },
                    {
                        "key": "t_ind"
                    },
                    {
                        "key": "next_earn"
                    },
                    {
                        "key": "t_sec"
                    },
                            {
                        "key": "ind_comp"
                    },
                            {
                        "key": "t_live"
                    },
                            {
                        "key": "t_descr"
                    },
                            {
                        "key": "is_primary_sec"
                    },
                            {
                        "key": "f_curr_code"
                    },
                            {
                        "key": "f_curr_name"
                    },
                            {
                        "key": "trading_schedule"
                    },
                            {
                        "key": "hq_city"
                    },
                            {
                        "key": "hq_state"
                    },
                            {
                        "key": "hq_fax"
                    },
                            {
                        "key": "f_compSet"
                    },
                            {
                        "key": "hq_website"
                    },
                            {
                        "key": "rel_ids"
                    },
                            {
                        "key": "year_founded"
                    },
                           {
                        "key": "t_sec"
                    },
                            {
                        "key": "etf_num"
                    },
                    {
                        "key": "etf_active"
                    },
                    {
                        "key": "etf_admin"
                    },
                    {
                        "key": "etf_advisor"
                    },
                    {
                        "key": "etf_asset_class"
                    },
                    {
                        "key": "etf_benchmark"
                    },
                    {
                        "key": "etf_cl"
                    },
                    {
                        "key": "etf_creation_fee"
                    },
                    {
                        "key": "etf_creation_size"
                    },
                    {
                        "key": "etf_custodian"
                    },
                    {
                        "key": "etf_dev_class"
                    },
                    {
                        "key": "etf_dist_freq"
                    },
                    {
                        "key": "etf_distributor"
                    },
                    {
                        "key": "etf_cat"
                    },
                    {
                        "key": "etf_fee_waivers"
                    },
                    {
                        "key": "etf_f_yr_end"
                    },
                    {
                        "key": "etf_focus"
                    },
                    {
                        "key": "etf_inception"
                    },
                    {
                        "key": "etf_is_levered"
                    },
                    {
                        "key": "etf_issuer"
                    },
                    {
                        "key": "etf_lev_amt"
                    },
                    {
                        "key": "etf_mgmt_fee"
                    },
                    {
                        "key": "etf_net_exp"
                    },
                    {
                        "key": "etf_port_manager"
                    },
                    {
                        "key": "etf_put_call_ratio"
                    },
                    {
                        "key": "etf_region"
                    },
                    {
                        "key": "etf_sub_advisor"
                    },
                    {
                        "key": "etf_tax_class"
                    },
                    {
                        "key": "etf_tot_aum"
                    },
                    {
                        "key": "etf_tot_exp"
                    },
                    {
                        "key": "etf_trans_agent"
                    }
                ]
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })