describe('POST API Equity Meta keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "eq-o1antf"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "t_n"
                    },
                    {
                        "key": "p_l"
                    },
                    {
                        "key": "t_cat"
                    },
                    {
                        "key": "company_country"
                    },
                    {
                        "key": "trading_country"
                    },
                    {
                        "key": "t_ex"
                    },
                    {
                        "key": "t_freq"
                    },
                    {
                        "key": "t_source"
                    },
                    {
                        "key": "t_u"
                    },
                    {
                        "key": "t_id"
                    },
                    {
                        "key": "t_ind"
                    },
                    {
                        "key": "next_earn"
                    },
                    {
                        "key": "t_sec"
                    },
                            {
                        "key": "ind_comp"
                    },
                            {
                        "key": "t_live"
                    },
                            {
                        "key": "t_descr"
                    },
                            {
                        "key": "is_primary_sec"
                    },
                            {
                        "key": "f_curr_code"
                    },
                            {
                        "key": "f_curr_name"
                    },
                            {
                        "key": "trading_schedule"
                    },
                            {
                        "key": "hq_city"
                    },
                            {
                        "key": "hq_state"
                    },
                            {
                        "key": "hq_fax"
                    },
                            {
                        "key": "f_compSet"
                    },
                            {
                        "key": "hq_website"
                    },
                            {
                        "key": "rel_ids"
                    },
                            {
                        "key": "year_founded"
                    },
                           {
                        "key": "t_sec"
                    }
                ]
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })