describe('POST API Equity Market keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "eq-o1antf"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "p_l"
                    },
                    {
                        "key": "p_b"
                    },
                    {
                        "key": "p_lupd"
                    },
                    {
                        "key": "avol"
                    },
                    {
                        "key": "vol"
                    },
                    {
                        "key": "pre_p"
                    },
                    {
                        "key": "post_p"
                    },
                    {
                        "key": "pre_vol"
                    },
                    {
                        "key": "pre_avol"
                    },
                    {
                        "key": "post_vol"
                    },
                    {
                        "key": "mar_state"
                    }
                ]
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })