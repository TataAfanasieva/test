describe('POST API Futures Common limits price keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "ft-zywvsh"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "p_min_1d"
                    },
                    {
                        "key": "p_max_1d"
                    },
                    {
                        "key": "low_52w"
                    },
                    {
                        "key": "high_52w"
                    }
                ],
                "currency": "USD"
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })