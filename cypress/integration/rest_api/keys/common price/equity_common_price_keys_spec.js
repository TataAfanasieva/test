describe('POST API Equity Common price keys', () => {
    it('POST successfully', () => {
        console.log(Cypress.env('BASE_URL'))
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/keys',
              body: {
                "ids": [
                    {
                        "type": "KID",
                        "id": "eq-o1antf"
                    }
                ],
                "keys": [
                    {
                        "key": "t"
                    },
                    {
                        "key": "t_n"
                    },
                    {
                        "key": "p_l"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "priceFormat": "adj",
                            "date": "2021-01-01"
                        },
                        "alias": "p_c_custom_2021-01-01_adj"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "priceFormat": "adj",
                            "date": "2020-01-01"
                        },
                        "alias": "p_c_custom_2020-01-01_adj"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "priceFormat": "adj",
                            "date": "2019-01-01"
                        },
                        "alias": "p_c_custom_2019-01-01_adj"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "priceFormat": "adj",
                            "date": "2018-01-01"
                        },
                        "alias": "p_c_custom_2018-01-01_adj"
                    },
                    {
                        "key": "p_b"
                    },
                    {
                        "key": "p_c1w"
                    },
                    {
                        "key": "p_c1m"
                    },
                    {
                        "key": "p_c3m"
                    },
                    {
                        "key": "p_c6m"
                    },
                    {
                        "key": "p_c1y"
                    },
                    {
                        "key": "p_c3y"
                    },
                    {
                        "key": "p_c5y"
                    },
                    {
                        "key": "p_c10y"
                    },
                    {
                        "key": "p_cmtd"
                    },
                    {
                        "key": "p_cqtd"
                    },
                    {
                        "key": "p_cytd"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "date": "2021-01-01"
                        },
                        "alias": "p_c_custom_2021-01-01"
                    },
                    {
                        "key": "p_c_custom",
                        "params": {
                            "date": "2020-01-01"
                        },
                        "alias": "p_c_custom_2020-01-01"
                    },
                    {
                        "key": "t_cat"
                    },
                    {
                        "key": "t_u"
                    },
                    {
                        "key": "t_id"
                    }
                ],
                "currency": "USD"
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {
  
              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).have.property( 'KID') // true
    
            cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
    
          })
          
     })
    })