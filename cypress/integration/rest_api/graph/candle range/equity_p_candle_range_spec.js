describe('ETF Historical graph', () => {
    it('POST', () => {
          cy.request({
              method: 'POST',
              url: Cypress.env('BASE_API_URL') + '/api/v3/data/graph',
              body: {
                "id": "eq-212q1o",
                "key": "p_candle_range",
                "useAdjustedPrice": true,
                "priceFormat": "both",
                "candleAggregationPeriod": "day",
                "dateFrom": "2020-03-04",
                "dateTo": "2025-02-04"
            },
              headers: {
                  'Cookie': 'cookieValueGoesHere',
                  'Content-Type':'application/json'
              }
          }).then((response) => {

              expect(response.status).to.eq(200)
              expect(response.body).to.not.be.null
              expect(response.body).to.have.property('graph');
              cy.log(response)
              cy.writeFile("cypress/fixtures/post_response.json", response.body)
          })
       
     })
    })