describe('The Login , Logout', () => {

    it ('login', () => {
  
      cy.visit(`${Cypress.env('BASE_URL')}/login`)
  
      cy.get('input[name="email"]').type('test@koyfin.com')
      
      cy.get('input[name="password"]').type('12345abc')
      
      cy.get('div#root button[type="submit"]').click()
      
      // should be redirected to /home
      cy.url().should('include', '/home')
  
      //UI should reflect that user being logged in
      cy.get('[class*="userIconContainer"]')
      .click()
      .should('be.visible') 
 
      // should log out
      cy.get('div').contains('Sign Out')
      .click()
      cy.url().should('include', '/login')
    })
  })
  