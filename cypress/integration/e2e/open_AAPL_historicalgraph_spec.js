describe('The historical graph', () => {

    it ('login', () => {
  
      cy.visit(`${Cypress.env('BASE_URL')}/login`)
  
      cy.get('input[name="email"]').type('test@koyfin.com')
      
      cy.get('input[name="password"]').type('12345abc')
      
      cy.get('div#root button[type="submit"]').click()
      
      // should be redirected to /home
      cy.url().should('include', '/home')
  
      // should click search console
      cy.get('div#guide-intro-cb > div.console__label___1TUQM')
      .click()
      
      
      // should type AAPL historical graph 
      cy.get('div.console-popup__search___11G4_ > input')
      .click()
      .type('AAPL')
      
      //should select first ticker 
      cy.get('div#cb-item-0 div.console-popup__value___DMZf7')
      .click()
     
      //should type G
      cy.get('div.console-popup__search___11G4_ > input')
      .click()
      .type('g')
      
      //should open graph 
      cy.get('div#cb-item-0 > div.console-popup__functionTitle___2Rg5I')
      .click()

      //should contain ticker identifier 
      cy.url().should('include', '/eq-o1antf')
      
    })
  })
  